//
//  CategoriesManager.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class CategoriesManager {
    func getCategories(order: String, callback: @escaping (APIClient.APIError?) -> Void) {
        CategoriesService().getCategories(order: order, success: { (categories) in
            self.saveCategoriesToDatabase(categories)
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    func saveCategoriesToDatabase(_ categories: [Category]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(categories, update: Realm.UpdatePolicy.modified)
        }
    }
    
    func getCategories() -> [Category]? {
        let realm = try? Realm()
        if let categories = realm?.objects(Category.self) {
            return Array(categories)
        }
        return nil
    }
    
}
