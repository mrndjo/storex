//
//  CartManager.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 16/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class CartManager {
    
    var cart: Cart? {
        let realm = try? Realm()
        return realm?.objects(Cart.self).first
    }
    
    func generateCartObject(callback: @escaping (APIClient.APIError?) -> Void) {
        CartService().generateUniqueId(success: { (cart) in
            self.saveCartToDatabase(cart: cart)
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    func addProductToCart(productID: Int, attributes: String, callback: @escaping (APIClient.APIError?) -> Void) {
        guard let cartID = cart?.cart_id else { return }
    
        CartService().addProductToCart(cartID: cartID, productID: productID, attributes: attributes, success: { (cartItems) in
            self.saveCartItemsToDatabase(cartItems)
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    func fetchTotalAmount(callback: @escaping (APIClient.APIError?) -> Void) {
        guard let cartID = cart?.cart_id else { return }

        CartService().getShoppingCartAmount(cartID: cartID, success: { (amount) in
            self.saveTotalAmountToDatabase(amount)
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    private func saveTotalAmountToDatabase(_ amount: String) {
        let realm = try? Realm()
        try? realm?.write {
            cart?.totalAmount = amount
        }
    }
    
    private func saveCartItemsToDatabase(_ cartItems: [CartItem]) {
        removeCartItemsFromDatabase()

        let realm = try? Realm()
        try? realm?.write {
            for cartItem in cartItems {
                cart?.cart_items.append(cartItem)
            }
        }
    }
    
    private func saveCartToDatabase(cart: Cart) {
        removeCartFromDatabase()
        
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(cart)
        }
    }
    
    func removeCartItemsFromDatabase() {
        let realm = try? Realm()
        if let cartItems = realm?.objects(CartItem.self) {
            try? realm?.write {
                realm?.delete(cartItems)
            }
        }
    }
    
    func removeCartFromDatabase() {
        removeCartItemsFromDatabase()
        
        let realm = try? Realm()
        if let carts = realm?.objects(Cart.self) {
            try? realm?.write {
                realm?.delete(carts)
            }
        }
    }
    
    func doesCartExists() -> Bool {
        if cart == nil {
            return false
        } else {
            return true
        }
    }
    
}
