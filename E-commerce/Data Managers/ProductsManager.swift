//
//  ProductsManager.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class ProductsManager {
    
    func fetchProducts(page: Int, limit: Int, callback: @escaping (APIClient.APIError?) -> Void) {
        ProductsService().getProducts(page: page, limit: limit, success: { (products) in
            self.saveProductsToDatabase(products)
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    func fetchProductDetails(productID: Int, callback: @escaping (APIClient.APIError?) -> Void) {
        ProductsService().getProductDetails(productID, success: { (product) in
            self.saveProductsToDatabase([product])
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }

    
    func fetchProductsByCategory(_ categoryID: Int, page: Int, limit: Int, callback: @escaping (APIClient.APIError?) -> Void) {
        ProductsService().getProductsByCategory(categoryID, page: page, limit: limit, success: { (products) in
            self.saveProductsToDatabase(products, categoryID: categoryID)
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    func saveProductsToDatabase(_ products: [Product], categoryID: Int? = nil) {
        let realm = try? Realm()
        try? realm?.write {
            for product in products {
                if let categoryID = categoryID, categoryID != -1 {
                    product.category_id = categoryID
                }
                realm?.add(product, update: Realm.UpdatePolicy.modified)
            }
        }
    }
    
    func getProducts(categoryID: Int? = nil) -> [Product]? {
        let realm = try? Realm()
        if let products = realm?.objects(Product.self).filter("category_id == %@", categoryID ?? -1) {
            return Array(products)
        }
        return nil
    }
    
    func getProductByID(_ id: Int) -> Product? {
        let realm = try? Realm()
        return realm?.object(ofType: Product.self, forPrimaryKey: id)
    }
    
    func removeAllProducts() {
        let realm = try? Realm()
        if let products = realm?.objects(Product.self) {
            realm?.delete(products)
        }
    }
    
}
