//
//  LoginManager.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class CustomerManager {
    
    func login(email: String, password: String, callback: @escaping (APIClient.APIError?) -> Void) {
        CustomerService().login(email: email, password: password, success: { (custumer) in
            self.saveCustomerToDatabase(customer: custumer)
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    func signup(email: String, password: String, name: String, callback: @escaping (APIClient.APIError?) -> Void) {
        CustomerService().signUp(email: email, password: password, name: name, success: { (_) in
            callback(nil)
        }) { (error) in
            callback(error)
        }
    }
    
    func saveCustomerToDatabase(customer: Customer) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(customer, update: Realm.UpdatePolicy.modified)
        }
    }
}
