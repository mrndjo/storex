//
//  ShopViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 10/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

protocol ShopViewModelCoordinatorDelegate: class {
    func navigateToAllProducts()
    func navigateToDiscountedProducts()
}

class ShopViewModel {
    
    weak var coordinatorDelegate: ShopViewModelCoordinatorDelegate?
    
    init(coordinatorDelegate: ShopViewModelCoordinatorDelegate) {
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func navigateToAllProducts() {
        coordinatorDelegate?.navigateToAllProducts()
    }
    
    func navigateToDiscountedProducts() {
        coordinatorDelegate?.navigateToDiscountedProducts()
    }
    
}
