//
//  ShopViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 10/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit
import AlamofireImage

class ShopViewController: BaseViewController<ShopViewModel> {

    @IBOutlet weak var menView: UIView!
    @IBOutlet weak var womenView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    private func setupUI() {
        let menTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(menRowSelected(_:)))
        let womenTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(menRowSelected(_:)))

        menView.addGestureRecognizer(menTapGestureRecognizer)
        womenView.addGestureRecognizer(womenTapGestureRecognizer)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc
    func menRowSelected(_ recognizer: UIGestureRecognizer) {
        viewModel.navigateToAllProducts()
    }
    
    @objc
    func womenRowSelected(_ recognizer: UIGestureRecognizer) {
        viewModel.navigateToAllProducts()
    }
    
    @IBAction func shopNowPressed(_ sender: UIButton) {
        viewModel.navigateToAllProducts()
    }
    
}
