//
//  FilterViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 13/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit
import MultiSlider


class FilterViewController: BaseViewController<FilterViewModel> {

    @IBOutlet weak var priceRangeLabel: UILabel!
    var priceSlider: MultiSlider = MultiSlider()
    @IBOutlet weak var applyFiltersButton: StorexBottomButton!
    @IBOutlet weak var priceSliderContainer: UIView!
    @IBOutlet weak var sizesStackView: UIStackView!
    
    var priceRange: (min: CGFloat, max: CGFloat) = (0, 100)
    var selectedSizeIndexArray = [0] {
        didSet {
            setupSizeStackView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        priceRangeLabel.text = "($\(priceRange.min) - $\(priceRange.max))"
        priceSlider.minimumValue = priceRange.min
        priceSlider.maximumValue = priceRange.max
        priceSlider.outerTrackColor = .darkGrayish
        priceSlider.tintColor = .orange
        priceSlider.snapStepSize = 10
        priceSlider.value = [priceRange.min, priceRange.max]
        priceSlider.thumbImage = UIImage(named: "Handle")
        priceSlider.orientation = .horizontal
        applyFiltersButton?.backgroundColor = UIColor.orange
        applyFiltersButton?.setTitleColor(.white, for: .normal)
        applyFiltersButton?.setTitle("APPLY FILTERES", for: .normal)
        priceSliderContainer?.addSubview(priceSlider)
        priceSlider.addTarget(self, action: #selector(sliderChanged(_:)), for: .valueChanged) // continuous changes
        priceSlider.addTarget(self, action: #selector(sliderDragEnded(_:)), for: . touchUpInside) // sent when drag ends
        setupSizeStackView()
    }
    
    func setupSizeStackView() {
        
        for (index, button) in sizesStackView.arrangedSubviews.enumerated() {
            guard let polygoneButton = button as? PolygoneButton else { return }

            if selectedSizeIndexArray.contains(index) {
                polygoneButton.borderColor = nil
                polygoneButton.setTitleColor(.white, for: .normal)
                polygoneButton.backgroundColor = .orange
            } else {
                polygoneButton.borderColor = .subTitle
                polygoneButton.setTitleColor(.subTitle, for: .normal)
                polygoneButton.backgroundColor = .white
            }
        }
    }
    
    @objc
    func sliderChanged(_ slider: MultiSlider) {
        priceRangeLabel.text = "($\(slider.value.first!) - $\(slider.value.last!))"
    }
    
    @objc
    func sliderDragEnded(_ slider: MultiSlider) {
        priceRangeLabel.text = "($\(slider.value.first!) - $\(slider.value.last!))"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        priceSlider.frame = priceSliderContainer.bounds
    }
    
    @IBAction func buttonPressed(_ sender: StorexBottomButton) {
        viewModel.applyFiltersButtonPressed()
    }

    @IBAction func sizeButtonTap(_ sender: PolygoneButton) {
        let selectedIndex = sizesStackView.arrangedSubviews.firstIndex(of: sender) ?? 0
        if let indexInArray = self.selectedSizeIndexArray.firstIndex(of: selectedIndex) {
            self.selectedSizeIndexArray.remove(at: indexInArray)
        } else {
            self.selectedSizeIndexArray.append(selectedIndex)
        }
    }
}
