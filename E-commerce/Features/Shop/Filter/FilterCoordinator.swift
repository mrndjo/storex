//
//  FilterCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 13/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class FilterCoordinator: BaseNavigationControllerCoordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = FilterViewController.instantiate(storyboardName: "Shop")
        let viewModel = FilterViewModel(coordinatorDelegate: self)
        vc.viewModel = viewModel
        vc.modalPresentationStyle = .overFullScreen
        navigationController.present(vc, animated: true, completion: nil)
    }
    
}

extension FilterCoordinator: FilterViewModelCoordinatorProtocol {
    
    func applyFiltersButtonPressed() {
        navigationController.topViewController?.dismiss(animated: true, completion: nil)
    }
}
