//
//  FilterViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 13/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation


protocol FilterViewModelCoordinatorProtocol: class {
    func applyFiltersButtonPressed()
}

class FilterViewModel {
    weak var coordinatorDelegate: FilterViewModelCoordinatorProtocol?
    
    init(coordinatorDelegate: FilterViewModelCoordinatorProtocol) {
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func applyFiltersButtonPressed() {
        coordinatorDelegate?.applyFiltersButtonPressed()
    }
}
