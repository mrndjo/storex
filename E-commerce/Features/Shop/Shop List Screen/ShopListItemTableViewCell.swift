//
//  ShopListItemTableViewCell.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ShopListItemTableViewCell: UITableViewCell {
    
    struct ViewModel {
        var productID: Int
        var imageName: String
        var productName: String
        var productOldPrice: Double
        var productNewPrice: Double
    }

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var newPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupUI(viewModel: ShopListItemTableViewCell.ViewModel) {
        self.productImageView?.image = UIImage(named: viewModel.imageName)
        self.productNameLabel?.text = viewModel.productName.uppercased()
        if viewModel.productNewPrice > 0 {
            self.newPriceLabel?.text = getFormatedPrice(viewModel.productNewPrice)
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: getFormatedPrice(viewModel.productOldPrice))
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            self.oldPriceLabel?.attributedText = attributeString
        } else {
            self.oldPriceLabel?.text = getFormatedPrice(viewModel.productOldPrice)
            self.newPriceLabel?.text = ""
        }
    }
    
    func getFormatedPrice(_ price: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.formatWidth = 2
        let priceString = formatter.string(from: NSNumber(value: price)) ?? "0.00"
        return "$\(priceString)"
    }

    
}
