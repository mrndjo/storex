//
//  ShopListCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ShopListCoordinator: BaseNavigationControllerCoordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = ShopListViewController.instantiate(storyboardName: "Shop")
        let viewModel = ShopListViewModel(coordinatorDelegate: self, controllerDelegate: vc)
        vc.viewModel = viewModel
        navigationController.pushViewController(vc, animated: true)
    }
    
}

extension ShopListCoordinator: ShopListViewModelCoordinatorDelegate {
    
    func productSelected(productID: Int) {
        let productCoordinator = ProductCoordinator(navigationController: navigationController, productID: productID)
        self.childCoordinators.append(productCoordinator)
        productCoordinator.start()
    }
    
    func filterSelected() {
        let filterCoordinator = FilterCoordinator(navigationController: navigationController)
        self.childCoordinators.append(filterCoordinator)
        filterCoordinator.start()
    }

}
