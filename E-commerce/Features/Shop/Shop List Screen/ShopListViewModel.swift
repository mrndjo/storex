//
//  ShopListViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

protocol ShopListViewModelCoordinatorDelegate: class {
    func productSelected(productID: Int)
    func filterSelected()
}

protocol ShopListViewModelControllerDelegate: class {
    func reloadProductsData()
    func reloadCategoriesData()
    func apiErrorHappened(_ error: APIClient.APIError)
}

class ShopListViewModel {
    enum CellType {
        case product(ShopListItemTableViewCell.ViewModel)
    }
    
    weak var coordinatorDelegate: ShopListViewModelCoordinatorDelegate?
    weak var controllerDelegate: ShopListViewModelControllerDelegate?
    var cells: [CellType] = []
    var categories: [Category] {
        return CategoriesManager().getCategories() ?? []
    }
    var selectedCategoryID: Int?
    
    init(coordinatorDelegate: ShopListViewModelCoordinatorDelegate, controllerDelegate: ShopListViewModelControllerDelegate) {
        self.coordinatorDelegate = coordinatorDelegate
        self.controllerDelegate = controllerDelegate
    }
    
    func getProducts(categoryID: Int?) -> [Product] {
        return ProductsManager().getProducts(categoryID: categoryID) ?? []
    }
    
    func setupCells() {
        cells.removeAll()
        
        for product in getProducts(categoryID: self.selectedCategoryID) {
            let cellViewModel = ShopListItemTableViewCell.ViewModel(productID: product.product_id, imageName: product.thumbnail ?? "", productName: product.name, productOldPrice: Double(product.price)!, productNewPrice: Double(product.discounted_price)!)
            cells.append(.product(cellViewModel))
        }
        
        controllerDelegate?.reloadProductsData()
    }
    
    func selectProduct(index: Int) {
        let cellModel = self.cells[index]
        switch cellModel {
        case .product(let cellViewModel):
            coordinatorDelegate?.productSelected(productID: cellViewModel.productID)
        }
    }
    
    func openFilter() {
        coordinatorDelegate?.filterSelected()
    }
    
    func fetchCategories() {
        CategoriesManager().getCategories(order: "category_id", callback: { (error) in
            if let error = error {
                self.controllerDelegate?.apiErrorHappened(error)
            } else {
                self.controllerDelegate?.reloadCategoriesData()
            }
        })
    }
    
    func fetchProducts(categoryID: Int?) {
        if let categoryID = categoryID {
            ProductsManager().fetchProductsByCategory(categoryID, page: 1, limit: 20, callback: onProductFetchCompletionHandler)
        } else {
            ProductsManager().fetchProducts(page: 1, limit: 20, callback: onProductFetchCompletionHandler)
        }
    }
    
    func onProductFetchCompletionHandler(error: APIClient.APIError?) {
        if let error = error {
            self.controllerDelegate?.apiErrorHappened(error)
        } else {
            self.setupCells()
        }
    }
    
}
