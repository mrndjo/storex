//
//  ShopListViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ShopListViewController: BaseViewController<ShopListViewModel> {

    @IBOutlet weak var segmentedControl: StorexSegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerSubtitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        registerCells()
        setupSegmentedControl()
        viewModel.fetchCategories()
        viewModel.fetchProducts(categoryID: nil)
        viewModel.setupCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(showTopRightButton: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigationBar(showTopRightButton: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    

    func setupNavigationBar(showTopRightButton: Bool) {
        if showTopRightButton {
            let image = UIImage(named: "icon-filter")
            tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItem.Style.plain, target: self, action: #selector(filterIconTapped))
        } else {
            tabBarController?.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc
    func filterIconTapped() {
        viewModel.openFilter()
    }
    
    func setupSegmentedControl() {
        segmentedControl.removeAllSegments()
        
        for category in viewModel.categories.reversed() {
            segmentedControl.insertSegment(withTitle: category.name, at: 0, animated: false)
        }

        segmentedControl.insertSegment(withTitle: "All Products", at: 0, animated: false)
        
        segmentedControl.selectedSegmentIndex = 0
    }
    
    func registerCells() {
        let cell = UINib(nibName: "ShopListItemTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "ShopListItemTableViewCell")
    }

    @IBAction func categorySelected(_ sender: StorexSegmentedControl) {
        let index = sender.selectedSegmentIndex
        var categoryID: Int? = nil
        if index > 0 {
            categoryID = viewModel.categories[index-1].category_id
        }
        viewModel.selectedCategoryID = categoryID
        viewModel.setupCells()
        viewModel.fetchProducts(categoryID: categoryID)
    }
}

extension ShopListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel.cells[indexPath.row]
        switch model {
        case .product(let cellViewModel):
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ShopListItemTableViewCell", for: indexPath) as? ShopListItemTableViewCell {
                cell.setupUI(viewModel: cellViewModel)
                
                cell.imageView?.image = nil
                let url = APIClient.baseImageURL + "/" + cellViewModel.imageName
                cell.imageView?.af_setImage(withURL: URL(string: url)!, placeholderImage: nil, imageTransition: UIImageView.ImageTransition.flipFromBottom(0.2), runImageTransitionIfCached: false, completion: { (image) in
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()
                })
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectProduct(index: indexPath.row)
    }
}

extension ShopListViewController: ShopListViewModelControllerDelegate {
    
    func apiErrorHappened(_ error: APIClient.APIError) {
        showApiError(error)
    }
    
    func reloadProductsData() {
        tableView.reloadData()
    }
    
    func reloadCategoriesData() {
        setupSegmentedControl()
    }
    
}
