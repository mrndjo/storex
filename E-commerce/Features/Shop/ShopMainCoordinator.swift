//
//  ShopMainCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 10/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ShopMainCoordinator: BaseNavigationControllerCoordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = ShopViewController.instantiate(storyboardName: "Shop")
        let viewModel = ShopViewModel(coordinatorDelegate: self)
        vc.viewModel = viewModel
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController.setViewControllers([vc], animated: true)
    }
    
}

extension ShopMainCoordinator: ShopViewModelCoordinatorDelegate {
    
    func navigateToAllProducts() {
        let productListCoordinator = ShopListCoordinator(navigationController: navigationController)
        childCoordinators.append(productListCoordinator)
        productListCoordinator.start()
    }
    
    func navigateToDiscountedProducts() {
        
    }
    
}
