//
//  ProductCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ProductCoordinator: Coordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    var productID: Int

    required init(navigationController: UINavigationController, productID: Int) {
        self.navigationController = navigationController
        self.productID = productID
    }
    
    func start() {
        let vc = ProductViewController.instantiate(storyboardName: "Product")
        let viewModel = ProductViewModel(coordinatorDelegate: self, controllerDelegate: vc, productID: productID)
        vc.viewModel = viewModel
        vc.modalPresentationStyle = .overFullScreen
        navigationController.present(vc, animated: true, completion: nil)
    }
    
}

extension ProductCoordinator: ProductViewModelCoordinatorProtocol {
    
    func closeButtonPressed() {
        self.navigationController.topViewController?.dismiss(animated: true, completion: nil)
    }
    
    func addToCartButtonPressed() {
        
    }
    
}
