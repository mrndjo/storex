//
//  ProductViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit
import ImageSlideshow

class ProductViewController: BaseViewController<ProductViewModel> {
    @IBOutlet weak var costumizeView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var addToCartButton: StorexBottomButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var closeButtonImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var imageSlideShow: ImageSlideshow!
    @IBOutlet weak var attributeViewContainer: UIView!
    var attributeView = ProductAttributesView.instanceFromNib()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModel.fetchProductDetails()
    }
    
    func setupUI() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.onCloseButtonPressed(_:)))
        self.closeButtonImageView?.addGestureRecognizer(tapGestureRecognizer)
        self.closeButtonImageView?.isUserInteractionEnabled = true
        
        guard let product = viewModel.getProduct() else {
            return
        }
        
        if Double(product.discounted_price)! > 0 {
            self.priceLabel?.text = viewModel.getFormatedPrice(Double(product.discounted_price)!)
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: viewModel.getFormatedPrice(Double(product.price)!))
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            self.oldPriceLabel?.attributedText = attributeString
        } else {
            self.oldPriceLabel?.text = viewModel.getFormatedPrice(Double(product.price)!)
            self.priceLabel?.text = ""
        }
        
        addToCartButton?.backgroundColor = UIColor.orange
        addToCartButton?.setTitleColor(.white, for: .normal)
        addToCartButton?.titleLabel?.font = UIFont(name: SourceSansPro.bold.fontName, size: 12)

        descriptionLabel?.text = product.productDescription
        productTitleLabel?.text = product.name.uppercased()
        
        var imageInputs = [InputSource]()
        if let image1 = product.image, let url1 = URL(string: APIClient.baseImageURL + "/" + image1) {
            imageInputs.append(AlamofireSource(url: url1))
        }
        if let image2 = product.image2, let url2 = URL(string: APIClient.baseImageURL + "/" + image2) {
            imageInputs.append(AlamofireSource(url: url2))
        }
        imageSlideShow.setImageInputs(imageInputs)
        addTapGestureToCustomizeView()
        setupAttributeView()
    }
    
    private func setupAttributeView() {
        attributeViewContainer?.isHidden = true
        attributeViewContainer?.isUserInteractionEnabled = false
        self.attributeViewContainer.addSubview(attributeView)
    }
    
    private func addTapGestureToCustomizeView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onCustomizeViewPressed(_:)))
        costumizeView?.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.attributeView.frame =  CGRect(x: attributeViewContainer.bounds.origin.x,
                                           y: attributeViewContainer.bounds.origin.y + 1,
                                           width: attributeViewContainer.bounds.width,
                                           height: attributeViewContainer.bounds.height - 1)
    }
    
    @objc
    func onCustomizeViewPressed(_ tapGestureRecognizer: UITapGestureRecognizer) {
        attributeView.setupSizeStackView()
        attributeViewContainer?.isHidden.toggle()
        attributeViewContainer?.isUserInteractionEnabled.toggle()
        
    }
    
    @objc
    func onCloseButtonPressed(_ tapGestureRecognizer: UITapGestureRecognizer) {
        viewModel?.coordinatorDelegate?.closeButtonPressed()
    }
    
    @IBAction func addToCartButtonPressed(_ sender: StorexBottomButton) {
        viewModel.addProductToCart()
    }
    
}

extension ProductViewController: ProductViewControllerDelegate {
    
    func reloadData() {
        self.setupUI()
    }
    
    func errorHappened(_ error: APIClient.APIError) {
        self.showApiError(error)
    }
    
}
