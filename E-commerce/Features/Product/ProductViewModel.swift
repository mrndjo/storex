//
//  ProductViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

protocol ProductViewModelCoordinatorProtocol: class {
    func closeButtonPressed()
    func addToCartButtonPressed()
}

protocol ProductViewControllerDelegate: class {
    func reloadData()
    func errorHappened(_ error: APIClient.APIError)
}

class ProductViewModel {
    weak var coordinatorDelegate: ProductViewModelCoordinatorProtocol?
    weak var controllerDelegate: ProductViewControllerDelegate?
    var productID: Int
    
    func getProduct() -> Product? {
        return ProductsManager().getProductByID(self.productID)
    }
    
    init(coordinatorDelegate: ProductViewModelCoordinatorProtocol, controllerDelegate: ProductViewControllerDelegate, productID: Int) {
        self.coordinatorDelegate = coordinatorDelegate
        self.controllerDelegate = controllerDelegate
        self.productID = productID
    }
    
    func getFormatedPrice(_ price: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.formatWidth = 2
        let priceString = formatter.string(from: NSNumber(value: price)) ?? "0.00"
        return "$\(priceString)"
    }
    
    func fetchProductDetails() {
        ProductsManager().fetchProductDetails(productID: productID) { (error) in
            if let error = error {
                self.controllerDelegate?.errorHappened(error)
            } else {
                self.controllerDelegate?.reloadData()
            }
        }
    }
    
    func addProductToCart() {
        if CartManager().doesCartExists() == false {
            generateCartID()
        } else {
            addProductToCartID()
        }
    }
    
    private func generateCartID() {
        CartManager().generateCartObject { (error) in
            if let error = error {
                self.controllerDelegate?.errorHappened(error)
            } else {
                self.addProductToCartID()
            }
        }
    }
    
    private func addProductToCartID() {
        CartManager().addProductToCart(productID: productID, attributes: "xs, red") { (error) in
            if let error = error {
                self.controllerDelegate?.errorHappened(error)
            } else {
                self.coordinatorDelegate?.closeButtonPressed()
            }
        }
    }
}
