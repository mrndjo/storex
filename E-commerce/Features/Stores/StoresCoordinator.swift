//
//  StoresCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class StoresCoordinator: BaseNavigationControllerCoordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = StoresViewController.instantiate(storyboardName: "Stores")
        let viewModel = StoresViewModel()
        vc.viewModel = viewModel
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController.setViewControllers([vc], animated: true)
    }
    
}
