//
//  StoresViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class StoresViewController: BaseViewController<StoresViewModel> {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerIconView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadCells()
        headerIconView?.storexView(lineWidth: 1, sides: 4, cornerRadius: 1)
        registerCells()
        viewModel.loadCells()
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.tableView?.reloadData()
    }
    
    func registerCells() {
        let cell = UINib(nibName: "StoreLocationTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "StoreLocationTableViewCell")
    }
    
}

extension StoresViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.cells[indexPath.row] {
        case .storeLocation(let vm):
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StoreLocationTableViewCell") as? StoreLocationTableViewCell {
                cell.setupUI(model: vm)
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let selectedRow = tableView.cellForRow(at: indexPath)
//        selectedRow?.contentView.backgroundColor = .orange
//    }
    
}
