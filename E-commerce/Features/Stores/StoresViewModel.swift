//
//  StoresViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

class StoresViewModel {
    
    enum CellType {
        case storeLocation(StoreLocationTableViewCell.ViewModel)
    }
    
    var cells = [CellType]()
    
    func loadCells() {
        cells.removeAll()
        
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))
        cells.append(.storeLocation(StoreLocationTableViewCell.ViewModel(storeName: "Storex Northlake Mall", storeDescription: "Walking distance: 4 min", storeDistance: "3.4")))


    }
    
}
