//
//  StoreLocationTableViewCell.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class StoreLocationTableViewCell: UITableViewCell {
    
    struct ViewModel {
        var storeName: String
        var storeDescription: String
        var storeDistance: String
    }

    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var storeDescription: UILabel!
    @IBOutlet weak var storeDistanc: UILabel!
    
    func setupUI(model: StoreLocationTableViewCell.ViewModel){
        self.storeName.text = model.storeName
        self.storeDescription.text = model.storeDescription
        self.storeDistanc.text = model.storeDistance
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected == true {
            self.contentView.backgroundColor = .orange
            self.storeDescription.textColor = .white
        } else {
            self.contentView.backgroundColor = .white
            self.storeDescription.textColor = .subTitle
        }
    }
    
}
