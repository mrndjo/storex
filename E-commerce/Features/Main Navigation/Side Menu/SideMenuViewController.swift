//
//  SideMenuViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewLeadingConstraint: NSLayoutConstraint!
    
    var viewModel = SideMenuViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
        setupTableView()
        registerCells()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let screenWidth = UIScreen.main.bounds.width
        collectionViewLeadingConstraint.constant = viewModel.getSideMenuLeading(screenWidth: screenWidth)
    }

    private func setupViewController() {
        self.view.backgroundColor = UIColor.darkGray
    }
    
    private func setupTableView() {
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func registerCells() {
        collectionView.register(UINib(nibName: "SideMenuGridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SideMenuGridCollectionViewCell")
        collectionView.register(UINib(nibName: "SideMenuItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SideMenuItemCollectionViewCell")
        collectionView.register(UINib(nibName: "SocialMediaCellCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SocialMediaCellCollectionViewCell")
    }
    
    private func navigateTo(_ navigationType: SideMenuViewModel.NavigationType) {
        switch navigationType {
        case .bag:
            switchTabBar(index: 2)
        case .shop:
            switchTabBar(index: 0)
        case .inspiration:
            switchTabBar(index: 1)
        case .stores:
            switchTabBar(index: 3)
        case .myAccount:
            switchTabBar(index: 4)
        case .customerSupport:
            switchTabBar(index: 4)
        case .logOut:
            logout()
        }
    }
    
    private func logout() {
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.appCoordinator?.start()
        }
    }
    
    private func switchTabBar(index: Int) {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            for coordinator in myDelegate.appCoordinator?.childCoordinators ?? [] {
                if let mainCoordinator = coordinator as? MainCoordinator {
                    let sideMenu =  mainCoordinator.sideMenuVC
                    sideMenu?.hideMenu()
                    ((sideMenu?.contentViewController as? UINavigationController)?.viewControllers.first as? TabBarViewController)?.selectedIndex = index
                }
            }
        }
    }
}

extension SideMenuViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cells[section].count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.cells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellType = viewModel.cells[indexPath.section][indexPath.row]
        
        switch cellType {
        case .gridMenu(let viewModel, _):
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SideMenuGridCollectionViewCell", for: indexPath) as? SideMenuGridCollectionViewCell {
                cell.setupCell(viewModel: viewModel)
                return cell
            }
        case .menuItem(let viewModel, _):
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SideMenuItemCollectionViewCell", for: indexPath) as? SideMenuItemCollectionViewCell {
                cell.setupCell(viewModel: viewModel)
                return cell
            }
        case .socialMedia:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SocialMediaCellCollectionViewCell", for: indexPath) as? SocialMediaCellCollectionViewCell {
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 layout collectionViewLayout: UICollectionViewLayout,
                                 sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellType = viewModel.cells[indexPath.section][indexPath.row]
        
        switch cellType {
        case .gridMenu:
            return CGSize(width: 149.5, height: 149.5)
        case .menuItem(_):
            return CGSize(width: 300, height: 50)
        case .socialMedia:
            return CGSize(width: 300, height: 85)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 1, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellType = viewModel.cells[indexPath.section][indexPath.row]
        
        switch cellType {
        case .gridMenu(_, let navigationType):
            navigateTo(navigationType)
        case .menuItem(_, let navigationType):
            navigateTo(navigationType)
        case .socialMedia:
            break
        }
    }
}
