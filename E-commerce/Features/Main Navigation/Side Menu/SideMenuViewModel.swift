//
//  File.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit
import SideMenuSwift

class SideMenuViewModel {
    
    enum NavigationType {
        case shop
        case bag
        case inspiration
        case stores
        case myAccount
        case customerSupport
        case logOut
    }
    
    enum CellType {
        case gridMenu(SideMenuGridCollectionViewCell.ViewModel, NavigationType)
        case menuItem(SideMenuItemCollectionViewCell.ViewModel, NavigationType)
        case socialMedia
    }
    
    var cells: [[CellType]] = [[]]
    
    init() {
        loadCells()
    }
    
    private func loadCells() {
        cells.removeAll()
        
        loadGridCells()
        loadItemsCells()
        loadSocialMediaCells()
    }
    
    private func loadGridCells() {
        //first row
        let shopCell = SideMenuGridCollectionViewCell.ViewModel(image: UIImage(named: "shopIcon"), title: "Shop")
        let bagCell = SideMenuGridCollectionViewCell.ViewModel(image: UIImage(named: "bagIcon"), title: "Bag")
        cells.append([.gridMenu(shopCell, .shop), .gridMenu(bagCell, .bag)])
        
        //second row
        let inspirationCell = SideMenuGridCollectionViewCell.ViewModel(image: UIImage(named: "inspirationIcon"), title: "Inspiration")
        let storesCell = SideMenuGridCollectionViewCell.ViewModel(image: UIImage(named: "storesIcon"), title: "Stores")
        cells.append([.gridMenu(inspirationCell, .inspiration), .gridMenu(storesCell, .stores)])
    }
    
    private func loadItemsCells() {
        let myAccountCell = SideMenuItemCollectionViewCell.ViewModel(title: "My Account")
        cells.append([.menuItem(myAccountCell, .myAccount)])
        
        let customerSupportCell = SideMenuItemCollectionViewCell.ViewModel(title: "Customer Support")
        cells.append([.menuItem(customerSupportCell, .customerSupport)])
        
        let logOutCell = SideMenuItemCollectionViewCell.ViewModel(title: "Log Out")
        cells.append([.menuItem(logOutCell, .logOut)])
    }
    
    private func loadSocialMediaCells() {
        cells.append([.socialMedia])
    }
    
    func getSideMenuLeading(screenWidth: CGFloat) -> CGFloat {
        let sideMenuWidth = SideMenuController.preferences.basic.menuWidth
        return screenWidth - sideMenuWidth
    }
}
