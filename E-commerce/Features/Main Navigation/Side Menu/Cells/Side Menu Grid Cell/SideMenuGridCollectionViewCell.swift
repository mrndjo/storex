//
//  SideMenuGridCollectionViewCell.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 13/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class SideMenuGridCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var viewModel: ViewModel? {
        didSet {
            label.text = viewModel?.title
            imageView.image = viewModel?.image
        }
    }
    
    func setupCell(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
}

extension SideMenuGridCollectionViewCell {
    class ViewModel {
        var image: UIImage?
        var title: String
        
        init(image: UIImage?, title: String) {
            self.image = image
            self.title = title
        }
    }
}
