//
//  SideMenuItemCollectionViewCell.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 13/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class SideMenuItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
    var viewModel: ViewModel? {
        didSet {
            label.text = viewModel?.title
        }
    }

    func setupCell(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
}

extension SideMenuItemCollectionViewCell {
    class ViewModel {
        var title: String
        
        init(title: String) {
            self.title = title
        }
    }
}
