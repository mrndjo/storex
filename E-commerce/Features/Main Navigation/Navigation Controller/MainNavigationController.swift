//
//  MainNavigationController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 10/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit

class MainNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesign()
    }
    
    private func setupDesign() {
        self.navigationBar.shadowImage = nil
    }
}
