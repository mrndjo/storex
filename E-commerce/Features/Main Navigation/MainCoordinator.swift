//
//  LoginCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit
import SideMenuSwift

//protocol MainCoordinatorProtocol: class {
//    func loginSuccessful(coordinator: LoginCoordinator)
//}

class MainCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var window: UIWindow
    var sideMenuVC: SideMenuController?
    
    //weak var delegate: LoginCoordinatorProtocol?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "MainNavigation", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "SideMenuController") as? SideMenuController {
            
            SideMenuController.preferences.basic.position = .sideBySide
            SideMenuController.preferences.animation.shadowAlpha = 0
            SideMenuController.preferences.basic.statusBarBehavior = .hideOnMenu
            sideMenuVC = vc
            window.rootViewController = vc
        }
    }
}

