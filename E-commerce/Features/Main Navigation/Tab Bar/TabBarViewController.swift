//
//  TabBarViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 10/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit

class TabBarViewController: UITabBarController {
    
    enum TabBarItem: Int {
        case shop = 0
        case inspiration = 1
        case bag = 2
        case stores = 3
        case more = 4
        
        var title: String {
            switch self {
            case .shop:
                return "Shop"
            case .inspiration:
                return "Inspiration"
            case .bag:
                return "Bag"
            case .stores:
                return "Stores"
            case .more:
                return "More"
            }
        }
        
        var imageName: String {
            switch self {
            case .shop:
                return "shop-ios"
            case .inspiration:
                return "inspiration-tab"
            case .bag:
                return "bag-tab"
            case .stores:
                return "stores-tab"
            case .more:
                return "more-tab"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        setupTabBarButtons()
        setupDesign()
    }
    
    //MARK:- Setting up screens
    func setupTabBarButtons() {
        var viewControllers = [UIViewController]()
        
        viewControllers = [getTabBarItem(.shop), getTabBarItem(.inspiration), getTabBarItem(.bag), getTabBarItem(.stores), getTabBarItem(.more)]
        self.setViewControllers(viewControllers, animated: false)
        self.tabBar.tintColor = .darkGray
    }
    
    func setBagEmptyController() {
        for controller in viewControllers! {
            if let navigationController = controller as? UINavigationController {
                let firstVC = navigationController.children.first
                if firstVC is BagViewController {
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        let noItemsInBagCoordinator = NoItemsCoordinator(navigationController: navigationController)
                        appDelegate.appCoordinator?.childCoordinators.append(noItemsInBagCoordinator)
                        noItemsInBagCoordinator.start()
                    }
                }
            }
        }
    }
    
    func setBagWithItemsController() {
        for controller in viewControllers! {
            if let navigationController = controller as? UINavigationController {
                let firstVC = navigationController.children.first
                if firstVC is NoBagItemsViewController {
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        let bagCoordinator = BagCoordinator(navigationController: navigationController)
                        appDelegate.appCoordinator?.childCoordinators.append(bagCoordinator)
                        bagCoordinator.start()
                    }
                }
            }
        }
    }
    
    private func getTabBarItem(_ item: TabBarItem) -> UIViewController {
        let navigationController = UINavigationController()
        
        switch item {
        case .shop:
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                let shopCoordinator = ShopMainCoordinator(navigationController: navigationController)
                appDelegate.appCoordinator?.childCoordinators.append(shopCoordinator)
                shopCoordinator.start()
            }
        case .inspiration:
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                let inspirationCoordinator = InspirationCoordinator(navigationController: navigationController)
                appDelegate.appCoordinator?.childCoordinators.append(inspirationCoordinator)
                inspirationCoordinator.start()
            }
        case .bag:
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                let noItemsInBagCoordinator = NoItemsCoordinator(navigationController: navigationController)
                appDelegate.appCoordinator?.childCoordinators.append(noItemsInBagCoordinator)
                noItemsInBagCoordinator.start()
            }
        case .stores:
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                let storesCoordinator = StoresCoordinator(navigationController: navigationController)
                appDelegate.appCoordinator?.childCoordinators.append(storesCoordinator)
                storesCoordinator.start()
            }
        case .more:
            break
        }
        
        let tabBarItem = createTabBarItem(title: item.title, imageName: item.imageName)
        navigationController.tabBarItem = tabBarItem
        return navigationController
    }
    

    private func createTabBarItem(title: String, imageName: String) -> UITabBarItem {
        let tabBarItem = UITabBarItem(title: title, image: UIImage(named: imageName), selectedImage: UIImage(named: imageName + "Selected"))
        tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
        return tabBarItem
    }
    
    private func setupDesign() {
        let myTitleView = UIImageView(image: UIImage(named: "navigationIcon"))
        myTitleView.contentMode = UIView.ContentMode.scaleAspectFit
        myTitleView.frame.origin = CGPoint(x: 0, y: 0)
        myTitleView.frame.size = CGSize(width: 10, height: 10)
        self.navigationItem.titleView = myTitleView
        
        let sideMenuButton = UIBarButtonItem(image: UIImage(named: "sideMenuOpenIcon"), style: .plain, target: self, action: #selector(showSideMenu))
        self.navigationItem.leftBarButtonItem = sideMenuButton
        self.navigationController?.navigationBar.tintColor = .darkGray
    }
    
    @objc
    func showSideMenu() {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            for coordinator in myDelegate.appCoordinator?.childCoordinators ?? [] {
                if let mainCoordinator = coordinator as? MainCoordinator {
                    let sideMenu =  mainCoordinator.sideMenuVC
                    if sideMenu?.isMenuRevealed == true {
                        sideMenu?.hideMenu()
                    } else {
                        sideMenu?.revealMenu()
                    }
                }
            }
        }
    }
}

extension TabBarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if CartManager().doesCartExists() {
            setBagWithItemsController()
        } else {
            setBagEmptyController()
        }
    }
}
