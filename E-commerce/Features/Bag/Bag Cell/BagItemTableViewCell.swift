//
//  BagItemTableViewCell.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 17/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class BagItemTableViewCell: UITableViewCell {
    
    struct ViewModel {
        var itemID: Int
        var productID: Int
        var imageName: String
        var title: String
        var oldPrice: Double
        var newPrice: Double
        var color: UIColor
        var sizeLabel: String
    }
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var newPriceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var colorView: UIImageView!
    @IBOutlet weak var sizeLabel: UILabel!

    func setupUI(viewModel: ViewModel) {
        let url = APIClient.baseImageURL + "/" + viewModel.imageName
        self.itemImageView?.af_setImage(withURL: URL(string: url)!)
        self.itemTitleLabel?.text = viewModel.title
        if viewModel.newPrice > 0 {
            self.newPriceLabel?.text = getFormatedPrice(viewModel.newPrice)
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: getFormatedPrice(viewModel.oldPrice))
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            self.oldPriceLabel?.attributedText = attributeString
        } else {
            self.oldPriceLabel?.text = getFormatedPrice(viewModel.oldPrice)
            self.newPriceLabel?.text = ""
        }
        
    }
    
    func getFormatedPrice(_ price: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.formatWidth = 2
        let priceString = formatter.string(from: NSNumber(value: price)) ?? "0.00"
        return "$\(priceString)"
    }
    
}
