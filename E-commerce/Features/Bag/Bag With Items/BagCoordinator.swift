//
//  BagCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 15/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class BagCoordinator: BaseNavigationControllerCoordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = BagViewController.instantiate(storyboardName: "Bag")
        let viewModel = BagViewModel(coordinatorDelegate: self, controllerDelegate: vc)
        vc.viewModel = viewModel
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController.setViewControllers([vc], animated: true)
    }
    
}

extension BagCoordinator: BagViewModelCoordinatorDelegate {
    
    func bagItemEdit(_ itemID: Int) {
        
    }
    
    func checkoutButtonPressed() {
        
    }
    
}
