//
//  BagViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 15/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

protocol BagViewModelCoordinatorDelegate: class {
    func checkoutButtonPressed()
    func bagItemEdit(_ itemID: Int)
}

protocol BagViewModelControllerDelegate: class {
    func reloadData()
    func errorHappened(_ error: APIClient.APIError)
}

class BagViewModel {
    
    enum CellType {
        case bagItemCell(BagItemTableViewCell.ViewModel)
    }
    
    weak var coordinatorDelegate: BagViewModelCoordinatorDelegate?
    weak var controllerDelegate: BagViewModelControllerDelegate?
    var cells = [CellType]()
    var bag: Cart? {
        return CartManager().cart
    }
    
    var titleSufix: String {
        return bag?.cart_items.count == 1 ? "item" : "items"
    }
    
    init(coordinatorDelegate: BagViewModelCoordinatorDelegate, controllerDelegate: BagViewModelControllerDelegate) {
        self.coordinatorDelegate = coordinatorDelegate
        self.controllerDelegate = controllerDelegate
    }
    
    func loadCells() {
        guard let bagItems = bag?.cart_items else { return }
        
        cells.removeAll()
        
        for bagItem in bagItems {
            let product = ProductsManager().getProductByID(bagItem.product_id)
            let cellViewModel = BagItemTableViewCell.ViewModel(itemID: bagItem.item_id, productID: bagItem.product_id, imageName: product?.thumbnail ?? "", title: bagItem.name, oldPrice: Double(bagItem.price)!, newPrice: Double(bagItem.price)!, color: .red, sizeLabel: "M")
            let cell = CellType.bagItemCell(cellViewModel)
            cells.append(cell)
        }
        
        controllerDelegate?.reloadData()
    }
    
    func editBagItem(_ itemID: Int) {
        coordinatorDelegate?.bagItemEdit(itemID)
    }
    
    func getTotalAmount() {
        CartManager().fetchTotalAmount { (error) in
            if let error = error {
                self.controllerDelegate?.errorHappened(error)
            } else {
                self.controllerDelegate?.reloadData()
            }
        }
    }
    
}
