//
//  BagViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 15/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class BagViewController: BaseViewController<BagViewModel> {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var shippingValueLabel: UILabel!
    @IBOutlet weak var subtotalValueLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        tableView?.delegate = self
        tableView?.dataSource = self
        viewModel.loadCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
        registerCell()
        viewModel.getTotalAmount()
        viewModel.loadCells()
    }
    
    private func registerCell() {
        let cell = UINib(nibName: "BagItemTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "BagItemTableViewCell")
    }
    
    private func setupUI() {
        guard let bag = viewModel.bag else { return }
        self.subtotalValueLabel.text = "$"
        self.shippingValueLabel.text = "$"
        self.totalValueLabel.text = "$" + bag.totalAmount
        
        self.titleLabel?.text = "You have \(bag.cart_items.count) \(viewModel.titleSufix) in your shopping bag."
        
    }
    
    @IBAction func checkouButtonPressed(_ sender: Any) {
        
    }
    
}

extension BagViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.cells[indexPath.row] {
        case .bagItemCell(let cellViewModel):
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BagItemTableViewCell", for: indexPath) as? BagItemTableViewCell {
                cell.setupUI(viewModel: cellViewModel)
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellViewModelType = viewModel.cells[indexPath.row]
        switch cellViewModelType {
        case .bagItemCell(let cellViewModel):
            viewModel.editBagItem(cellViewModel.itemID)
        }
    }
    
}

extension BagViewController: BagViewModelControllerDelegate {
    
    func reloadData() {
        setupUI()
        tableView?.reloadData()
    }
    
    func errorHappened(_ error: APIClient.APIError) {
        self.showApiError(error)
    }

}
