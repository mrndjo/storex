//
//  NoItemsCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 15/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class NoItemsCoordinator: BaseNavigationControllerCoordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = NoBagItemsViewController.instantiate(storyboardName: "Bag")
        let viewModel = NoBagItemsViewModel()
        vc.viewModel = viewModel
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController.setViewControllers([vc], animated: true)
    }
    
}
