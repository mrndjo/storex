//
//  InspirationsViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class InspirationsViewController: BaseViewController<InspirationsViewModel> {

    @IBOutlet weak var lifeButton: UIButton!
    @IBOutlet weak var fashionButton: UIButton!
    @IBOutlet weak var videosButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
    }

    @IBAction func lifeButtonPressed(_ sender: Any) {
    }
    
    @IBAction func fashionButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func videosButtonPressed(_ sender: Any) {
        
    }
    
    
}
