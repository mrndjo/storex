//
//  InspirationsCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 14/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class InspirationCoordinator: BaseNavigationControllerCoordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = InspirationsViewController.instantiate(storyboardName: "Inspirations")
        let viewModel = InspirationsViewModel()
        vc.viewModel = viewModel
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController.setViewControllers([vc], animated: true)
    }
    
}
