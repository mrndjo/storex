//
//  ResetPasswordViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

protocol ResetPasswordViewModelCoordinatorDelegate: class {
    func resetPasswordFinished()
}

class ResetPasswordViewModel {
    
    weak var coordinatorDelegate: ResetPasswordViewModelCoordinatorDelegate?
    
    init(coordinatorDelegate: ResetPasswordViewModelCoordinatorDelegate?) {
        self.coordinatorDelegate = coordinatorDelegate
    }
    
}
