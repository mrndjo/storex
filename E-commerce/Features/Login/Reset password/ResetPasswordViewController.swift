//
//  ResetPasswordViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ResetPasswordViewController: BaseViewController<ResetPasswordViewModel> {

    @IBOutlet weak var polygoneView: UIView!
    @IBOutlet weak var resetPasswordButton: StorexBottomButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailContainerView: UIView!
    private let emailTextFieldContainer = TextFieldContainerView.instanceFromNib()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .lightGrayish
        self.title = "RESET PASSWORD"
        
        titleLabel?.text = "Enter your registered Storex password and we will email you a link to reset your password."
        titleLabel?.textColor = .navigationTitle
        titleLabel?.font = UIFont(name: SourceSansPro.light.fontName, size: 18)
        
        emailContainerView?.addSubview(emailTextFieldContainer)
        emailTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: "icon-mail", title: "Email"))
        
        resetPasswordButton?.backgroundColor = UIColor.orange
        resetPasswordButton?.setTitleColor(.white, for: .normal)
        resetPasswordButton?.setTitle("RESET PASSWORD", for: .normal)
        
        polygoneView?.backgroundColor = UIColor.orange
        polygoneView?.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.emailTextFieldContainer.frame = emailContainerView.bounds
    }
    
    @IBAction func resetPasswordButtonPressed(_ sender: StorexBottomButton) {
        
    }
    
}
