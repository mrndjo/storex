//
//  ResetPasswordCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ResetPasswordCoordinator: BaseNavigationControllerCoordinator {
    
    var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    var childCoordinators: [Coordinator] = []
    
    func start() {
        let resetPasswordController = ResetPasswordViewController.instantiate(storyboardName: "Login")
        let resetPassViewModel = ResetPasswordViewModel(coordinatorDelegate: self)
        resetPasswordController.viewModel = resetPassViewModel
        self.navigationController.pushViewController(resetPasswordController, animated: true)
    }
    
}

extension ResetPasswordCoordinator: ResetPasswordViewModelCoordinatorDelegate {
    
    func resetPasswordFinished() {
        
    }
    
}
