//
//  SignUpCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class SignUpCoordinator: BaseNavigationControllerCoordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = SignUpViewController.instantiate(storyboardName: "Login")
        let viewModel = SignUpViewModel(coordinatorDelegate: self, controllerDelegate: vc)
        vc.viewModel = viewModel
        self.navigationController.pushViewController(vc, animated: true)
    }
    
}

extension SignUpCoordinator: SignUpViewModelCoordinatorDelegate {
    func userDidSignUp() {
        
    }
    
    func goToLoginScreen() {
        // TODO: Remove coordinator
        navigationController.popViewController(animated: true)
    }
    
    
}
