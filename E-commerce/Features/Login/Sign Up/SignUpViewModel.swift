//
//  SignUpViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

protocol SignUpViewModelCoordinatorDelegate: class {
    func userDidSignUp()
    func goToLoginScreen()
}

protocol SignUpViewControllerDelegate: class {
    func errorHappened(_ error: APIClient.APIError)
    func userIsRegistered()
    func validationFailed()
}

class SignUpViewModel {
    
    weak var coordinatorDelegate: SignUpViewModelCoordinatorDelegate?
    weak var controllerDelegate: SignUpViewControllerDelegate?
    var isMaleSelected: Bool = true
    
    init(coordinatorDelegate: SignUpViewModelCoordinatorDelegate, controllerDelegate: SignUpViewControllerDelegate) {
        self.coordinatorDelegate = coordinatorDelegate
        self.controllerDelegate = controllerDelegate
    }
    
    func goBackToLogin() {
        coordinatorDelegate?.goToLoginScreen()
    }
    
    func signUpUser(email: String, password: String, retypedPassword: String, name: String, termsAccepted: Bool) {
        if !termsAccepted || email.isEmpty || password.isEmpty || name.isEmpty || (password != retypedPassword) {
            controllerDelegate?.validationFailed()
            return
        }
        
        CustomerManager().signup(email: email, password: password, name: name) { (error) in
            if let error = error {
                self.controllerDelegate?.errorHappened(error)
            } else {
                self.controllerDelegate?.userIsRegistered()
            }
        }
    }

}
