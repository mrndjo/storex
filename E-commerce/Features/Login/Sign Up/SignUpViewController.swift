//
//  SignUpViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController<SignUpViewModel> {

    @IBOutlet weak var firstNameContainer: UIView!
    @IBOutlet weak var lastNameContainer: UIView!
    @IBOutlet weak var passwordContainer: UIView!
    @IBOutlet weak var retypePasswordContainer: UIView!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var dayContainer: UIView!
    @IBOutlet weak var monthContainer: UIView!
    @IBOutlet weak var yearContainer: UIView!
    @IBOutlet weak var selectYourGanderLabel: UILabel!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var termsSwitch: UISwitch!
    @IBOutlet weak var signUpButton: StorexBottomButton!
    @IBOutlet weak var polygoneView: UIView!
    @IBOutlet weak var alreadyMemberLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private let firstNameTextFieldContainer =  TextFieldContainerView.instanceFromNib()!
    private let lastNameTextFieldContainer =  TextFieldContainerView.instanceFromNib()!
    private let passwordTextFieldContainer =  TextFieldContainerView.instanceFromNib()!
    private let retypePasswordTextFieldContainer =  TextFieldContainerView.instanceFromNib()!
    private let dayTextFieldContainer =  TextFieldContainerView.instanceFromNib()!
    private let monthTextFieldContainer =  TextFieldContainerView.instanceFromNib()!
    private let yearTextFieldContainer =  TextFieldContainerView.instanceFromNib()!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        registerObservers()
    }
    
    func registerObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    @objc
    func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            scrollView?.contentInset = UIEdgeInsets(top: CGFloat.zero, left: CGFloat.zero, bottom: keyboardSize.height, right: CGFloat.zero)
        }
    }
    
    @objc
    func keyboardWillHide(_ notification: Notification) {
        scrollView?.contentInset = UIEdgeInsets.zero
    }
    
    func setupUI() {
        self.view.backgroundColor = .lightGrayish
        self.title = "SIGN UP"
        
        dateOfBirthLabel?.text = "DATE OF BIRTH"
        dateOfBirthLabel?.textColor = UIColor.subTitle
        dateOfBirthLabel?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)
        
        selectYourGanderLabel?.text = "SELECT YOUR GENDER"
        selectYourGanderLabel?.textColor = UIColor.subTitle
        selectYourGanderLabel?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)
        
        termsLabel?.text = "I AGREE THE TERMS OF USE"
        termsLabel?.textColor = UIColor.subTitle
        termsLabel?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)

        signUpButton?.backgroundColor = UIColor.orange
        signUpButton?.setTitleColor(.white, for: .normal)
        signUpButton?.setTitle("SIGN UP", for: .normal)
        
        maleButton?.titleLabel?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)
        maleButton?.setTitleColor(.navigationTitle, for: .normal)
        maleButton?.setTitle("Male", for: .normal)
        
        femaleButton?.titleLabel?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)
        femaleButton?.setTitleColor(.subTitle, for: .normal)
        femaleButton?.setTitle("Female", for: .normal)

        firstNameContainer?.addSubview(firstNameTextFieldContainer)
        lastNameContainer?.addSubview(lastNameTextFieldContainer)
        passwordContainer?.addSubview(passwordTextFieldContainer)
        retypePasswordContainer?.addSubview(retypePasswordTextFieldContainer)
        dayContainer?.addSubview(dayTextFieldContainer)
        monthContainer?.addSubview(monthTextFieldContainer)
        yearContainer?.addSubview(yearTextFieldContainer)

        firstNameTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: nil, title: "First Name"))
        lastNameTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: nil, title: "Email"))
        passwordTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: nil, title: "Password"))
        retypePasswordTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: nil, title: "Retype Password"))
        dayTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: nil, title: "DD"))
        monthTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: nil, title: "MM"))
        yearTextFieldContainer.setupUI(model: TextFieldContainerView.Model(imageName: nil, title: "JJ"))

        passwordTextFieldContainer.textField?.isSecureTextEntry = true
        retypePasswordTextFieldContainer.textField?.isSecureTextEntry = true
        
        polygoneView?.backgroundColor = UIColor.orange
        polygoneView?.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)

        termsSwitch?.thumbTintColor = .darkGray
        termsSwitch?.onTintColor = .lightesGray
        
        alreadyMemberLabel?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)
        alreadyMemberLabel?.textColor = UIColor.navigationTitle
        alreadyMemberLabel?.text = "Already a member?"
        
        signInButton?.titleLabel?.font = UIFont(name: SourceSansPro.bold.fontName, size: 15)
        signInButton?.setTitleColor(.orange, for: .normal)
        signInButton?.setTitle("SIGN IN", for: .normal)
        
        termsSwitch.isOn = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstNameTextFieldContainer.frame = firstNameContainer.bounds
        lastNameTextFieldContainer.frame = lastNameContainer.bounds
        passwordTextFieldContainer.frame = passwordContainer.bounds
        retypePasswordTextFieldContainer.frame = retypePasswordContainer.bounds
        dayTextFieldContainer.frame = dayContainer.bounds
        monthTextFieldContainer.frame = monthContainer.bounds
        yearTextFieldContainer.frame = yearContainer.bounds
    }
    
    @IBAction func maleButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func femaleButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func termsSwitchChanged(_ sender: UISwitch) {
        
    }
    
    @IBAction func signUpButtonPressed(_ sender: StorexBottomButton) {
        viewModel.signUpUser(email: lastNameTextFieldContainer.textFieldText, password: passwordTextFieldContainer.textFieldText, retypedPassword: retypePasswordTextFieldContainer.textFieldText, name: firstNameTextFieldContainer.textFieldText, termsAccepted: termsSwitch.isOn)
    }
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        viewModel.goBackToLogin()
    }
}

extension SignUpViewController: SignUpViewControllerDelegate {
    
    func errorHappened(_ error: APIClient.APIError) {
        self.showApiError(error)
    }
    
    func userIsRegistered() {
        let alertVC = UIAlertController(title: "Success", message: "Signup success", preferredStyle: UIAlertController.Style.alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func validationFailed() {
        let alertVC = UIAlertController(title: "Oops! Something went wrong", message: "Required fields: email, name, password. Password must be the same as the retyped password. You need to accept terms of use.", preferredStyle: UIAlertController.Style.alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
    
}
