//
//  LoginViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

enum SocialNetworkType {
    case facebook
    case twitter
}

protocol LoginViewModelCoordinatorDelegate: class {
    func loginSuccessful()
    func forgotPasswordSelected()
    func socialNetworkSelected(type: SocialNetworkType)
    func signUpSelected()
}

protocol LoginViewControllerDelegate: class {
    func errorHappened(_ error: APIClient.APIError)
}

class LoginViewModel {

    weak var coordinatorDelegate: LoginViewModelCoordinatorDelegate?
    weak var controllerDelegate: LoginViewControllerDelegate?
    
    init(coordinatorDelegate: LoginViewModelCoordinatorDelegate, controllerDelegate: LoginViewControllerDelegate?) {
        self.coordinatorDelegate = coordinatorDelegate
        self.controllerDelegate = controllerDelegate
    }
    
    func performLogin(email: String, password: String) {
        CustomerManager().login(email: email, password: password) {  [weak self] (error) in
            if let error = error {
                self?.controllerDelegate?.errorHappened(error)
            } else {
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                self?.coordinatorDelegate?.loginSuccessful()
            }
        }
    }
    
    func goToSignUpScreen() {
        coordinatorDelegate?.signUpSelected()
    }
    
    func goToForgotPasswordScreen() {
        coordinatorDelegate?.forgotPasswordSelected()
    }
    
    func goToSocialNetworkLogin(type: SocialNetworkType) {
        
    }
}
