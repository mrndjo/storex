//
//  LoginCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit

protocol LoginCoordinatorProtocol: class {
    func loginSuccessful(coordinator: LoginCoordinator)
}

class LoginCoordinator: BaseNavigationControllerCoordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    weak var delegate: LoginCoordinatorProtocol?

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = LoginViewController.instantiate(storyboardName: "Login")
        let viewModel = LoginViewModel(coordinatorDelegate: self, controllerDelegate: vc)
        vc.viewModel = viewModel
        navigationController.pushViewController(vc, animated: true)
    }
    
}

extension LoginCoordinator: LoginViewModelCoordinatorDelegate {
    
    func forgotPasswordSelected() {
        let resetPasswordCoordinator = ResetPasswordCoordinator(navigationController: navigationController)
        self.childCoordinators.append(resetPasswordCoordinator)
        resetPasswordCoordinator.start()
    }
    
    func socialNetworkSelected(type: SocialNetworkType) {
        
    }
    
    func signUpSelected() {
        let signupCoordinator = SignUpCoordinator(navigationController: navigationController)
        self.childCoordinators.append(signupCoordinator)
        signupCoordinator.start()
    }
    
    func loginSuccessful() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let mainCoordinator = MainCoordinator(window: appDelegate.window!)
            appDelegate.appCoordinator?.childCoordinators.append(mainCoordinator)
            mainCoordinator.start()
        }
    }
    
}
