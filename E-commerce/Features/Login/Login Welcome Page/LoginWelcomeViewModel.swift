//
//  LoginWelcomeViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

protocol LoginWelcomeViewModelCoordinatorDelegate: class {
    func signInButtonPressed()
}

class LoginWelcomeViewModel {
    var title = "Follow the latest fashion trends\n and shop with storex!"
    var buttonTitle = "SIGN IN"
    weak var coordinateDelegate: LoginWelcomeViewModelCoordinatorDelegate?
    
    init(coordinateDelegate: LoginWelcomeViewModelCoordinatorDelegate?) {
        self.coordinateDelegate = coordinateDelegate
    }
    
    func openLoginPage() {
        coordinateDelegate?.signInButtonPressed()
    }
}
