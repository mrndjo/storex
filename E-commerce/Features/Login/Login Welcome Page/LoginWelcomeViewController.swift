//
//  LoginWelcomeViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit


class LoginWelcomeViewController: BaseViewController<LoginWelcomeViewModel> {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var signInButton: StorexBottomButton!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        self.titleLabel?.font = UIFont(name: SourceSansPro.light.fontName, size: 18)
        self.titleLabel?.text = viewModel.title
        self.signInButton?.backgroundColor = UIColor.orange
        self.signInButton?.setTitleColor(UIColor.white, for: .normal)
        self.signInButton?.setTitle(viewModel.buttonTitle, for: .normal)
        imageView?.image = UIImage(named: "singnin_image")
    }
    
    @IBAction func signInButtonPressed(_ sender: StorexBottomButton) {
        viewModel.openLoginPage()
    }
    
}
