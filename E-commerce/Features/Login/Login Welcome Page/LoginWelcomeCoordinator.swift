//
//  LoginWelcomeCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class LoginWelcomeCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var window: UIWindow
    private var navigationController: UINavigationController!
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let vc = LoginWelcomeViewController.instantiate(storyboardName: "Login")
        navigationController = UINavigationController(rootViewController: vc)
        let viewModel = LoginWelcomeViewModel(coordinateDelegate: self)
        vc.viewModel = viewModel
        window.rootViewController = navigationController
    }

}

extension LoginWelcomeCoordinator: LoginWelcomeViewModelCoordinatorDelegate {
    
    func signInButtonPressed() {
        let loginCoordinator = LoginCoordinator(navigationController: navigationController)
        self.childCoordinators.append(loginCoordinator)
        loginCoordinator.start()
    }
    
}
