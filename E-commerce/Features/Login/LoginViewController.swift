//
//  LoginViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController<LoginViewModel> {

    @IBOutlet weak var emailContainer: UIView!
    @IBOutlet weak var passwordContainer: UIView!
    @IBOutlet weak var signInButton: StorexBottomButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var polygoneView: UIView!
    @IBOutlet weak var socialNetworksLabel: UILabel!
    @IBOutlet weak var notMemberLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var facebookContainer: UIView!
    @IBOutlet weak var twitterContainer: UIView!
    
    private let emailTextFieldContainer: TextFieldContainerView! = TextFieldContainerView.instanceFromNib()
    private let passwordTextFieldContainer: TextFieldContainerView! = TextFieldContainerView.instanceFromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        self.title = "SIGN IN"
        self.view.backgroundColor = UIColor.lightGrayish
        
        emailContainer?.addSubview(emailTextFieldContainer)
        emailTextFieldContainer?.setupUI(model: TextFieldContainerView.Model(imageName: "icon-mail", title: "Email"))

        passwordContainer?.addSubview(passwordTextFieldContainer)
        passwordTextFieldContainer?.setupUI(model: TextFieldContainerView.Model(imageName: "icon-password", title: "Password"))
        passwordTextFieldContainer?.textField?.isSecureTextEntry = true
        
        signInButton?.backgroundColor = UIColor.orange
        signInButton?.setTitleColor(.white, for: .normal)
        signInButton?.setTitle("SIGN IN", for: .normal)
        
        emailTextFieldContainer?.textField?.delegate = self
        passwordTextFieldContainer?.textField?.delegate = self
        
        forgotPasswordButton?.backgroundColor = .clear
        forgotPasswordButton?.titleLabel?.font = UIFont(name: SourceSansPro.light.fontName, size: 15)
        forgotPasswordButton?.setTitleColor(UIColor.navigationTitle, for: .normal)
        forgotPasswordButton?.setTitle("Forgot password?", for: .normal)
        
        polygoneView?.backgroundColor = UIColor.orange
        polygoneView?.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
        
        socialNetworksLabel?.text = "OR, SIGN IN USING SOCIAL NETWORKS"
        socialNetworksLabel?.font = UIFont(name: SourceSansPro.light.fontName, size: 15)
        socialNetworksLabel?.textColor = UIColor.navigationTitle
        
        notMemberLabel?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)
        notMemberLabel?.textColor = UIColor.navigationTitle
        notMemberLabel?.text = "Not a member?"
        
        signUpButton?.titleLabel?.font = UIFont(name: SourceSansPro.bold.fontName, size: 15)
        signUpButton?.setTitleColor(.orange, for: .normal)
        signUpButton?.setTitle("SIGN UP", for: .normal)
        
        facebookContainer.storexView(lineWidth: 1, sides: 6, cornerRadius: 1)
        twitterContainer.storexView(lineWidth: 1, sides: 6, cornerRadius: 1)
    }
    
    @IBAction func signInPressed(_ sender: StorexBottomButton) {
        viewModel.performLogin(email: emailTextFieldContainer.textFieldText, password: passwordTextFieldContainer.textFieldText)
    }
    
    @IBAction func forgotPasswordButton(_ sender: UIButton) {
        viewModel.goToForgotPasswordScreen()
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        viewModel.goToSignUpScreen()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailTextFieldContainer?.frame = emailContainer?.bounds ?? .zero
        passwordTextFieldContainer?.frame = passwordContainer?.bounds ?? .zero
    }
    
}

extension LoginViewController: LoginViewControllerDelegate {
    
    func errorHappened(_ error: APIClient.APIError) {
        showApiError(error)
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
