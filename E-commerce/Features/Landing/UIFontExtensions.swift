//
//  UIFontExtensions.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

enum SourceSansPro: String {
    case black = "Black"
    case blackitalic = "BlackItalic"
    case bold = "Bold"
    case bolditalic = "BoldItalic"
    case extralight = "ExtraLight"
    case extralightitalic = "ExtraLightItalic"
    case italic = "Italic"
    case light = "Light"
    case lightitalic = "LightItalic"
    case regular = "Regular"
    case semibold = "SemiBold"
    case semibolditalic = "SemiBoldItalic"
    
    var fontName: String {
        return "SourceSansPro-\(self.rawValue)"
    }
}

enum AdventPro: String {
    case bold = "Bold"
    case extralight = "ExtraLight"
    case light = "Light"
    case medium = "Medium"
    case regular = "Regular"
    case semibold = "SemiBold"
    case thin = "Thin"
    
    var fontName: String {
        return "AdventPro-\(self.rawValue)"
    }
}

