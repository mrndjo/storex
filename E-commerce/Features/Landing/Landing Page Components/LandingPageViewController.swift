//
//  LandingPageViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class LandingPageViewController: BasePageViewController<LandingPageViewModel> {

    private var continueButton = StorexBottomButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        if let firstVC = viewModel.orderedViewControllers.first {
            self.setViewControllers([firstVC], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
        
        addContinueButton()
    }
    
    private func addContinueButton() {
        continueButton.backgroundColor = UIColor.darkGray
        continueButton.setTitleColor(UIColor.white, for: .normal)
        continueButton.setTitle(viewModel.buttonTitle, for: .normal)
        continueButton.setImage(UIImage(named: "forwardIcon"), for: .normal)
        continueButton.addTarget(self, action: #selector(self.didTapContinueButton), for: UIControl.Event.touchUpInside)
        
        
        self.view.addSubview(continueButton)
        self.view.bringSubviewToFront(continueButton)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.continueButton.frame = CGRect(x: 0, y: self.view.frame.maxY - viewModel.buttonHeight - self.view.safeAreaInsets.bottom, width: self.view.frame.width, height: viewModel.buttonHeight)
    }
    
    @objc
    func didTapContinueButton() {
        viewModel.landingFlowFinished()
    }
    
}

extension LandingPageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
       
        guard let viewControllerIndex = viewModel.orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
             return nil
        }
        
        guard viewModel.orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return viewModel.orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = viewModel.orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = viewModel.orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
             return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return viewModel.orderedViewControllers[nextIndex]
    }
    
}
