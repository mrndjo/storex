//
//  LandingCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit

protocol LandingPageCoordinatorDelegate: class {
    func landingDismissed()
}

class LandingPageCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var window: UIWindow
    weak var delegate: LandingPageCoordinatorDelegate?
    
    init(window: UIWindow, delegate: LandingPageCoordinatorDelegate) {
        self.window = window
        self.delegate = delegate
    }
    
    func start() {
        let pageViewController = LandingPageViewController.instantiate(storyboardName: "Landing")
        let viewModel = LandingPageViewModel(coordinatorDelegate: self)
        pageViewController.viewModel = viewModel
        window.rootViewController = pageViewController
    }

}

extension LandingPageCoordinator: LandingPageViewModelCoordinatorDelegate {
    
    func landingFlowFinished() {
        let loginWelcomeCoordinator = LoginWelcomeCoordinator(window: window)
        childCoordinators.append(loginWelcomeCoordinator)
        loginWelcomeCoordinator.start()
    }
    
}
