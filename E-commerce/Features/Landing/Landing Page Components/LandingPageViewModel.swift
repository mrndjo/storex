//
//  LandingPageViewModel.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

protocol LandingPageViewModelCoordinatorDelegate: class {
    func landingFlowFinished()
}

class LandingPageViewModel {
    
    weak var coordinatorDelegate: LandingPageViewModelCoordinatorDelegate?
    
    var buttonTitle = "CONTINUE"
    var buttonHeight: CGFloat = 55
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.getVC(),
        self.getVC(),
        self.getVC()]
    }()
    
    init(coordinatorDelegate: LandingPageViewModelCoordinatorDelegate?) {
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // This method could be used as a factory for landing view controllers
    private func getVC() -> UIViewController {
        let vc = LandingViewController.instantiate(storyboardName: "Landing")
        return vc
    }

    func landingFlowFinished() {
        coordinatorDelegate?.landingFlowFinished()
    }
}
