//
//  UIColorExtensions.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(rgbValue: UInt) {
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static var darkGray: UIColor = UIColor(rgbValue: 0x454545)
    static var orange: UIColor = UIColor(rgbValue: 0xEFB961)
    static var lightGrayish: UIColor = UIColor(rgbValue: 0xF2F2F2)
    static var navigationTitle: UIColor = UIColor(rgbValue: 0x454545)
    static var lineTextField: UIColor = UIColor(rgbValue: 0xC4C4C4)
    static var subTitle: UIColor = UIColor(rgbValue: 0x999999)
    static var lightesGray: UIColor = UIColor(rgbValue: 0xEBEBEB)
    static var manBlue: UIColor = UIColor(rgbValue: 0x93D6DC)
    static var darkGrayish: UIColor = UIColor(rgbValue: 0xA2A2A2)
    static var navigationSeparator: UIColor = UIColor(rgbValue: 0x585858)
}
