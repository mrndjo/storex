//
//  StorexSegmentedControl.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class StorexSegmentedControl: UISegmentedControl {

    var currentIndex: Int = 0
    var selectedSegmentColor: UIColor = UIColor.manBlue {
        didSet {
            configure()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        self.backgroundColor = UIColor(rgbValue: 0xF2F2F2)
        self.tintColor = .clear
        if let fontForNormalState = UIFont(name: SourceSansPro.regular.fontName, size: 14),
            let fontForSelectedState = UIFont(name: SourceSansPro.regular.fontName, size: 14) {
            self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.navigationTitle, NSAttributedString.Key.font: fontForNormalState], for: .normal)
            self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedSegmentColor, NSAttributedString.Key.font: fontForSelectedState], for: .selected)
            
            self.apportionsSegmentWidthsByContent = true
            
        }
    }

}

