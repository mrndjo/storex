//
//  TextFieldContainerView.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class TextFieldContainerView: UIView {
    
    struct Model {
        var imageName: String?
        var title: String
    }

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: StorexOnboardingTextField!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLeadingConstraint: NSLayoutConstraint!
    
    var textFieldTextDidChange: ((String) -> Void)?
    
    var textFieldText: String {
        return textField?.text ?? ""
    }
    
    static func instanceFromNib() -> TextFieldContainerView? {
        return UINib(nibName: "TextFieldContainerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? TextFieldContainerView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        runInitialViewSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        runInitialViewSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        runInitialViewSetup()
    }
    
    private func runInitialViewSetup() {
        textField?.addTarget(self, action: #selector(textFieldTextDidChange(_:)), for: UIControl.Event.editingChanged)
        label?.font = UIFont(name: SourceSansPro.regular.fontName, size: 15)
        label?.textColor = UIColor.navigationTitle
    }
    
    func setupUI(model: TextFieldContainerView.Model) {
        if let imageName = model.imageName {
            self.imageView?.image = UIImage(named: imageName)
        } else {
            imageWidthConstraint?.constant = 0
            titleLeadingConstraint?.constant = 0
        }
        self.label?.text = model.title
        self.label?.textColor = UIColor.navigationTitle
    }

    @objc
    func textFieldTextDidChange(_ textField: UITextField) {
        textFieldTextDidChange?(textFieldText)
    }
    
}
