//
//  ProductAttributesView.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 20/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class ProductAttributesView: UIView {

    var selectedSizeIndex = 0 {
        didSet {
            setupSizeStackView()
        }
    }
    
    @IBOutlet weak var sizesStackView: UIStackView!
    
    static func instanceFromNib() -> ProductAttributesView? {
        return UINib(nibName: "ProductAttributesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ProductAttributesView
    }

    @IBAction func sizeButtonPressed(_ sender: PolygoneButton) {
        let selectedIndex = sizesStackView.arrangedSubviews.firstIndex(of: sender) ?? 0
        self.selectedSizeIndex = selectedIndex
    }
    
    @IBAction func colorButtonPressed(_ sender: PolygoneButton) {
        
    }
    
    func setupSizeStackView() {
        
        for (index, button) in sizesStackView.arrangedSubviews.enumerated() {
            guard let polygoneButton = button as? PolygoneButton else { return }
            
            if selectedSizeIndex == index {
                polygoneButton.borderColor = nil
                polygoneButton.setTitleColor(.white, for: .normal)
                polygoneButton.backgroundColor = .orange
            } else {
                polygoneButton.borderColor = .subTitle
                polygoneButton.setTitleColor(.subTitle, for: .normal)
                polygoneButton.backgroundColor = .white
            }
        }
    }
    
}
