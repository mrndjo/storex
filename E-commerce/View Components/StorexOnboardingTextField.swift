//
//  StorexOnboardingTextField.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class StorexOnboardingTextField: UITextField {
    
    private let border = CALayer()
    private let borderWidth: CGFloat = 2

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupBorder()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupBorder()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    private func setupBorder() {
        self.borderStyle = .none
        border.borderColor = UIColor.lineTextField.cgColor
        border.borderWidth = borderWidth
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
}
