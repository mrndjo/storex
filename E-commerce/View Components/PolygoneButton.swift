//
//  PolygoneButton.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 13/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class PolygoneButton: UIButton {
    
    var borderColor: UIColor? = nil {
        didSet {
            setupUI()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.storexView(lineWidth: 1, sides: 6, cornerRadius: 1, borderColor: borderColor ?? .white)
        self.titleLabel?.font = UIFont(name: SourceSansPro.bold.fontName, size: 12)
    }
}
