//
//  StorexBottomButton.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

class StorexBottomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.titleLabel?.font = UIFont(name: SourceSansPro.bold.fontName, size: 15)
        self.semanticContentAttribute = .forceRightToLeft
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        self.imageView?.contentMode = .scaleAspectFit
    }

}
