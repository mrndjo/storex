//
//  APIClient.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import KeychainAccess

enum Endpoint {
    case login
    case signup
    case categories
    case products
    case productsByCategory(Int)
    case productDetails(Int)
    case generateUniqueId
    case addProductToCart
    case getShoppingCartProducts(String)
    case totalCartAmount(String)
    
    var path: String {
        switch self {
        case .login:
            return "/customers/login"
        case .signup:
            return "/customers"
        case .categories:
            return "/categories"
        case .products:
            return "/products"
        case .productsByCategory(let categoryID):
            return "/products/inCategory/\(categoryID)"
        case .productDetails(let productID):
            return "/products/\(productID)"
        case .generateUniqueId:
            return "/shoppingcart/generateUniqueId"
        case .addProductToCart:
            return "/shoppingcart/add"
        case .getShoppingCartProducts(let cartID):
            return "/shoppingcart/\(cartID)"
        case .totalCartAmount(let cartID):
            return "/shoppingcart/totalAmount/\(cartID)"
        }
    }
}


class APIClient {
    
    static let baseURL = "https://mobilebackend.turing.com"
    static let baseImageURL = "https://mobilebackend.turing.com/images/products"
    
    static func makeRequest(endpoint: Endpoint, method: HTTPMethod, parameters: Parameters?, success: @escaping (Data) -> Void, failure: @escaping (APIError) -> Void) {

        let header  = HTTPHeaders(dictionaryLiteral: ("USER-KEY", SessionManager.getToken() ?? ""))
        let url = baseURL + endpoint.path
        var destination: URLEncoding.Destination = .httpBody
        if method == .get {
            destination = .queryString
        }
        

        let _ = Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding(destination: destination), headers: header).validate().responseJSON { (response) in
            let data = response.data!
            switch response.result {
            case .success(_):
                saveApiToken(data: data)
                success(data)
            case .failure(let error):
                do {
                    let apiErrorResponse = try JSONDecoder().decode(APIErrorResponse.self, from: data)
                    failure(apiErrorResponse.error)
                } catch {
                    let apiError = APIError()
                    failure(apiError)
                }
            }
            
        }
    }
    
    static func getImage(name: String, success: @escaping (Image) -> Void, failure: @escaping (APIError) -> Void) {
        let url = APIClient.baseImageURL + "/" + name
        
        Alamofire.request(url).validate().responseImage { (response) in
            if let image = response.result.value {
                success(image)
            } else {
                failure(APIError())
            }
        }
    }
    
    static func saveApiToken(data: Data) {
        do {
            let key = try JSONDecoder().decode(APIKey.self, from: data)
            if let accessToken = key.accessToken {
                SessionManager.saveToken(accessToken)
            }
        } catch {
            
        }
    }
}

class SessionManager {
    
    static func getToken() -> String? {
        let keychain = Keychain(service: "com.turing.E-commerce")
        return keychain["APIKEY"]
    }
    
    static func saveToken(_ token: String) {
        let keychain = Keychain(service: "com.turing.E-commerce")
        keychain["APIKEY"] = token
    }
    
}

extension APIClient {
    class APIErrorResponse: Codable {
        var error: APIError
    }
    
    struct APIError: Codable {
        var status: Int
        var code: String?
        var message: String?
        var field: String?
        
        init() {
            self.status = 400
            self.message = "Bad response"
        }
    }
}
