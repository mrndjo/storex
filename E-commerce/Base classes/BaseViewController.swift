//
//  BaseViewController.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController<ViewModelType>: UIViewController, Storyboarded {
    var viewModel: ViewModelType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButtonDesign()
    }
    
    func setBackButtonDesign() {
        
        guard let navigationBar = navigationController?.navigationBar else {
            return
        }
        
        let backArrowImage = UIImage(named: "icon-back")?.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        navigationBar.backgroundColor = .clear
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: .default)
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont(name: SourceSansPro.regular.fontName, size: 18)!, NSAttributedString.Key.foregroundColor : UIColor.navigationTitle]
        navigationBar.backIndicatorImage = UIImage()
        navigationBar.backIndicatorTransitionMaskImage = UIImage()
        navigationBar.tintColor = UIColor.navigationTitle
        let backButton = UIBarButtonItem(image: backArrowImage, style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
    }
    
    func showApiError(_ error: APIClient.APIError) {
        let alertVC = UIAlertController(title: "Oops! Something went wrong", message: error.message, preferredStyle: UIAlertController.Style.alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
}

class BasePageViewController<ViewModelType>: UIPageViewController, Storyboarded {
    var viewModel: ViewModelType!
}
