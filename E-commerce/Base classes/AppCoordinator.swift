//
//  AppCoordinator.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 08/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit
import Foundation

class AppCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            let mainCoordinator = MainCoordinator(window: window)
            childCoordinators.append(mainCoordinator)
            mainCoordinator.start()
        } else {
            let landingPageCoordinator = LandingPageCoordinator(window: window, delegate: self)
            childCoordinators.append(landingPageCoordinator)
            landingPageCoordinator.start()
        }
    }
    
}

extension AppCoordinator: LoginCoordinatorProtocol {
    
    func loginSuccessful(coordinator: LoginCoordinator) {
        if let index = childCoordinators.firstIndex(where: { $0 === coordinator }) {
            childCoordinators.remove(at: index)
        }
    }
    
}

extension AppCoordinator: LandingPageCoordinatorDelegate {
    
    func landingDismissed() {
        
    }
}
