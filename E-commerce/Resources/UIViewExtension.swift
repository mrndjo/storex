//
//  UIViewExtension.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 10/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import UIKit

extension UIView {
    
    func storexView(lineWidth: CGFloat, sides: NSInteger, cornerRadius: CGFloat, rotationOffset: CGFloat = 0, borderColor: UIColor = UIColor.clear) {
        
        let path = UIBezierPath()
        let theta: CGFloat = CGFloat(2.0 * Double.pi) / CGFloat(sides)
        let width = min(bounds.size.width, bounds.size.height)
        
        let center = CGPoint(x: bounds.origin.x + width / 2.0, y: bounds.origin.y + width / 2.0)
        
        let radius = (width - lineWidth + cornerRadius - (cos(theta) * cornerRadius)) / 2.0
        
        var angle = CGFloat(rotationOffset)
        
        let corner = CGPoint(x: center.x + (radius - cornerRadius) * cos(angle), y: center.y + (radius - cornerRadius) * sin(angle))
        path.move(to: CGPoint(x: corner.x + cornerRadius * cos(angle + theta), y: corner.y + cornerRadius * sin(angle + theta)))
        
        for _ in 0..<sides {
            angle += theta
            
            let corner = CGPoint(x: center.x + (radius - cornerRadius) * cos(angle), y: center.y + (radius - cornerRadius) * sin(angle))
            let tip = CGPoint(x: center.x + radius * cos(angle), y: center.y + radius * sin(angle))
            let start = CGPoint(x: corner.x + cornerRadius * cos(angle - theta), y: corner.y + cornerRadius * sin(angle - theta))
            let end = CGPoint(x: corner.x + cornerRadius * cos(angle + theta), y: corner.y + cornerRadius * sin(angle + theta))
            
            path.addLine(to: start)
            path.addQuadCurve(to: end, controlPoint: tip)
        }
        
        path.close()
        
        let bounds = path.bounds
        let transform = CGAffineTransform(translationX: -bounds.origin.x + bounds.origin.x + lineWidth / 2.0, y: -bounds.origin.y + bounds.origin.y + lineWidth / 2.0)
        path.apply(transform)
        
        
        
        let lineWidth: CGFloat = 5
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.lineWidth = lineWidth
        mask.strokeColor = UIColor.clear.cgColor
        mask.fillColor = UIColor.white.cgColor
        self.layer.mask = mask
        
        let border = CAShapeLayer()
        border.path = path.cgPath
        border.lineWidth = 1
        border.strokeColor = borderColor.cgColor
        border.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(border)
        
    }
    
}
