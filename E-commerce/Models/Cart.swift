//
//  Cart.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 16/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class Cart: Object, Codable {
    
    @objc dynamic var cart_id: String = ""
    @objc dynamic var totalAmount: String = ""
    let cart_items = List<CartItem>()
    
    enum CodingKeys: String, CodingKey {
        case cart_id
    }
    
    override static func primaryKey() -> String? {
        return "cart_id"
    }
    
}

class CartItem: Object, Codable {
    @objc dynamic var item_id: Int = 0
    @objc dynamic var product_id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var attributes: String = ""
    @objc dynamic var price: String = "0.0"
    @objc dynamic var quantity: Int = 0
    @objc dynamic var subtotal: String = "0.0"
    
    override static func primaryKey() -> String? {
        return "item_id"
    }
}
