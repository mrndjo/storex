//
//  Product.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 10/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class Product: Object, Codable {
    @objc dynamic var product_id: Int = 0
    @objc dynamic var category_id: Int = -1
    @objc dynamic var name: String = ""
    @objc dynamic var productDescription: String = ""
    @objc dynamic var price: String = "0.0"
    @objc dynamic var discounted_price: String = "0.0"
    @objc dynamic var thumbnail: String?
    @objc dynamic var image: String?
    @objc dynamic var image2: String?
    
    enum CodingKeys: String, CodingKey {
        case product_id
        case name
        case productDescription = "description"
        case price
        case discounted_price
        case thumbnail
        case image
        case image2 = "image_2"
    }
    
    override static func primaryKey() -> String? {
        return "product_id"
    }
}
