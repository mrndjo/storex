//
//  ProductsResponse.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

class ProductsResponse: Codable {
    var rows: [Product]
    var count: Int
}
