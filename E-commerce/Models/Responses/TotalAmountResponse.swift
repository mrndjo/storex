//
//  TotalAmountResponse.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 22/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

class TotalAmountResponse: Codable {
    var total_amount: String
}
