//
//  CustomerLoginResponse.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation

class CustomerLoginResponse: Codable {
    var user: Customer
}

class CustomerSignUpResponse: Codable {
    var customer: Customer
}
