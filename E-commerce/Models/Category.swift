//
//  Category.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object, Codable {
    @objc dynamic var category_id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var categoryDescription: String = ""
    @objc dynamic var department_id: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case category_id
        case name
        case categoryDescription = "description"
        case department_id
    }
    
    override static func primaryKey() -> String? {
        return "category_id"
    }
}
