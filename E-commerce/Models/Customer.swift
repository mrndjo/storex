//
//  Customer.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import RealmSwift

class Customer: Object, Codable {
    @objc dynamic var customer_id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var address_1: String?
    @objc dynamic var address_2: String?
    @objc dynamic var city: String?
    @objc dynamic var region: String?
    @objc dynamic var postal_code: String?
    @objc dynamic var shipping_region_id: Int = 0
    @objc dynamic var day_phone: String?
    @objc dynamic var eve_phone: String?
    @objc dynamic var mob_phone: String?
    
    override static func primaryKey() -> String? {
        return "customer_id"
    }
}

struct APIKey: Codable {
    var accessToken: String?
    var expires_in: String?
}
