//
//  ProductsService.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import Alamofire

class ProductsService {
    
    func getProducts(page: Int, limit: Int, success: @escaping ([Product]) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        let parameters = Parameters(dictionaryLiteral: ("page", page), ("limit", limit))
        APIClient.makeRequest(endpoint: .products, method: .get, parameters: parameters, success: { (response) in
            do {
                let customerResponse = try JSONDecoder().decode(ProductsResponse.self, from: response)
                success(customerResponse.rows)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getProductsByCategory(_ categoryID: Int, page: Int, limit: Int, success: @escaping ([Product]) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        let parameters = Parameters(dictionaryLiteral: ("page", page), ("limit", limit))
        APIClient.makeRequest(endpoint: .productsByCategory(categoryID), method: .get, parameters: parameters, success: { (response) in
            do {
                let customerResponse = try JSONDecoder().decode(ProductsResponse.self, from: response)
                success(customerResponse.rows)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }

    func getProductDetails(_ productID: Int, success: @escaping (Product) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        APIClient.makeRequest(endpoint: .productDetails(productID), method: .get, parameters: nil, success: { (response) in
            do {
                let product = try JSONDecoder().decode(Product.self, from: response)
                success(product)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
}

