//
//  LoginService.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 09/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import Alamofire

class CustomerService {
    
    func login(email: String, password: String, success: @escaping (Customer) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        let parameters = Parameters(dictionaryLiteral: ("email", email), ("password", password))
        APIClient.makeRequest(endpoint: .login, method: .post, parameters: parameters, success: { (response) in
            do {
                let customerResponse = try JSONDecoder().decode(CustomerLoginResponse.self, from: response)
                success(customerResponse.user)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func signUp(email: String, password: String, name: String, success: @escaping (Customer) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        let parameters = Parameters(dictionaryLiteral: ("email", email), ("password", password), ("name", name))
        APIClient.makeRequest(endpoint: .signup, method: .post, parameters: parameters, success: { (response) in
            do {
                let customerResponse = try JSONDecoder().decode(CustomerSignUpResponse.self, from: response)
                success(customerResponse.customer)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
}
