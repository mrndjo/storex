//
//  CartService.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 16/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import Alamofire


class CartService {
    
    func generateUniqueId(success: @escaping (Cart) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        APIClient.makeRequest(endpoint: .generateUniqueId, method: .get, parameters: nil, success: { (response) in
            do {
                let cart = try JSONDecoder().decode(Cart.self, from: response)
                success(cart)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func addProductToCart(cartID: String, productID: Int, attributes: String, success: @escaping ([CartItem]) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        let parameters = Parameters(dictionaryLiteral: ("cart_id", cartID), ("product_id", productID), ("attributes", attributes))
        APIClient.makeRequest(endpoint: .addProductToCart, method: .post, parameters: parameters, success: { (response) in
            do {
                let productsResponse = try JSONDecoder().decode([CartItem].self, from: response)
                success(productsResponse)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getShoppingCartProducts(cartID: String, success: @escaping ([CartItem]) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        APIClient.makeRequest(endpoint: .getShoppingCartProducts(cartID), method: .get, parameters: nil, success: { (response) in
            do {
                let productsResponse = try JSONDecoder().decode([CartItem].self, from: response)
                success(productsResponse)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getShoppingCartAmount(cartID: String, success: @escaping (String) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        APIClient.makeRequest(endpoint: .totalCartAmount(cartID), method: .get, parameters: nil, success: { (response) in
            do {
                let totalAmount = try JSONDecoder().decode(TotalAmountResponse.self, from: response)
                success(totalAmount.total_amount)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
}
