//
//  CategoriesService.swift
//  E-commerce
//
//  Created by Almedin Mrndic on 11/06/2019.
//  Copyright © 2019 Almedin Mrndic. All rights reserved.
//

import Foundation
import Alamofire

class CategoriesService {
    
    func getCategories(order: String, success: @escaping ([Category]) -> Void, failure: @escaping (APIClient.APIError) -> Void) {
        let parameteres = Parameters(dictionaryLiteral: ("orded", order))
        APIClient.makeRequest(endpoint: .categories, method: .get, parameters: parameteres, success: { (response) in
            do {
                let categoriesResponse = try JSONDecoder().decode(CategoriesResponse.self, from: response)
                success(categoriesResponse.rows)
            } catch {
                failure(APIClient.APIError())
            }
        }) { (error) in
            failure(error)
        }
    }
    
}
